import java.io.BufferedWriter;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger 
{	
	
	static private Logger loggerObj = null;
	private BufferedWriter writer = null;
		
	private Logger()
	{
		try{
			String logFileName = "log"+Utv_batchimport_push.isdevStr+".txt";
			writer = new BufferedWriter(new FileWriter(logFileName, true));
			writeLog("============== Program Start ==============", false);
		} catch(Exception e){
			System.out.println(e.getMessage());
		}
	}
	
	public static Logger getInstance()
	{
		try{
			if(loggerObj == null)
				loggerObj = new Logger();			
		} catch(Exception e){
			System.out.println(e.getMessage());
		}
		return loggerObj;
	}
	public void writeLog(String msg)
	{
		writeLog(msg, true);
	}
	public void writeLog(String msg, boolean withDate)
	{
		try{
			Date currentTime = new Date();
			String formatTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(currentTime);			
			if ( withDate)	writer.write("["+formatTime+"] ");	
			writer.write(msg);
			writer.newLine();
			writer.flush();
		} catch(Exception e){
			System.out.println(e.getMessage());
		}
	}
}
