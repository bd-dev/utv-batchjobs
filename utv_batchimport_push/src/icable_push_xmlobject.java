import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class icable_push_xmlobject {

	public static SimpleDateFormat xmlFormat = new SimpleDateFormat("yyyyMMddHHmm");

	private String xmlFilename;
	private String Provider, LastUpdateDate, PushID, PushContent, VODID, Category;

	public static SimpleDateFormat getXmlFormat() {
		return xmlFormat;
	}

	public icable_push_xmlobject() {
	}
	public icable_push_xmlobject(String filename, Document doc) {
		try {
			this.xmlFilename = filename;
			
			Element mainNode = (Element) (doc.getElementsByTagName("AppPush").item(0)); // only 1

			Provider = mainNode.getElementsByTagName("Provider").item(0).getTextContent();
			LastUpdateDate = mainNode.getElementsByTagName("LastUpdateDate").item(0).getTextContent();
			PushID = mainNode.getElementsByTagName("PushID").item(0).getTextContent();
			PushContent = mainNode.getElementsByTagName("PushContent").item(0).getTextContent();
			VODID = mainNode.getElementsByTagName("VODID").item(0).getTextContent();
			Category = mainNode.getElementsByTagName("Category").item(0).getTextContent();
//			Date lastupdateDateObj = xmlFormat.parse(LastUpdateDate);

		} catch (Exception O) {
		}

	}

	public String toString() {
		String msg = " Push " + getPushID();
		msg += " sent at " + getLastUpdateDate();
		msg += ",  Message= " + getPushContent();
		msg += "\n";

		return msg;
	}
	
	public String getXmlFilename() {
		return xmlFilename;
	}
	
	public String getProvider() {
		return Provider;
	}

	public String getLastUpdateDate() {
		return LastUpdateDate;
	}

	public String getPushID() {
		return PushID;
	}

	public String getPushContent() {
		return PushContent;
	}

	public String getVODID() {
		return VODID;
	}

	public String getCategory() {
		return Category;
	}

	/*
	 * D:\NVideo\iCable-App_push\5066.xml
	 * 
	 * <?xml version="1.0" encoding="UTF-8"?> <AppPush> <Provider>iCable</Provider>
	 * <LastUpdateDate>20180319130225</LastUpdateDate> <PushID>5066</PushID>
	 * <PushContent><![CDATA[《視像推介》愛女被偷拍樣貌曝光 城城嬲爆：小朋友需要保護！]]></PushContent>
	 * <VODID>496788</VODID> <Category>Local</Category> </AppPush>
	 */

}
