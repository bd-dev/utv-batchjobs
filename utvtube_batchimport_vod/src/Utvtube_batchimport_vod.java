
import java.net.URI;
import java.net.URL;
import java.io.BufferedReader;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.Calendar;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;


public class Utvtube_batchimport_vod {
	
	public class categories_id  {	
		static final int icable_news	= 11; 
		static final int  icable_finance = 12;
		static final int icable_entertainment = 13;
	};

	private static String incomingFolderName = "Incoming";
	private static String processedFolderName = "Processed";
	private static String skippedFolderName = "Skipped";
	private static String failedFolderName = "Failed";
	private static String[] folderRequired = { incomingFolderName, processedFolderName, skippedFolderName, failedFolderName };

	
	private static SimpleDateFormat icableformat = new SimpleDateFormat("yyyyMMddHHmmss");

	public static String isdevStr = "_vod";
	private static String api_server_url = "";
	private static String ftp_folder_path = "";
	private static int housekeep_days = 9999;

	public static Logger logger = null;

	private static Date dateCutoff = new Date();

	static ArrayList<Icable_xml_object> xmlreadList;
	static Map<Integer, Utvtube_video_object> existingVid = new HashMap<Integer, Utvtube_video_object>();

	public static void main(String[] args) {

		// program initialize
		logger = Logger.getInstance();
		xmlreadList = new ArrayList<Icable_xml_object>();

		logger.writeLog("Utvtube batch import start.");
		// read config
		try {
			BufferedReader brReader = new BufferedReader(new FileReader("config-vod.txt"));
			String strRow = brReader.readLine();
			while (strRow != null) {
				String[] result = strRow.split("=");
				if (result.length >= 2) {
					String pname = result[0];
					String pvalue = result[1];
					if (pname.compareTo("ENV") == 0) {
					//	isdevStr = pvalue;
					} else if (pname.compareTo("API_SERVER_URL") == 0) {
						api_server_url = pvalue;
					} else if (pname.compareTo("FTP_FOLDER_PATH") == 0) {
						ftp_folder_path = pvalue;
					} else if (pname.compareTo("HOUSEKEEP_DAYS") == 0) {
						housekeep_days = Integer.parseInt(pvalue);
					}
				}
				strRow = brReader.readLine();
			}
			brReader.close();
		} catch (FileNotFoundException O) {
			logger.writeLog("config.txt is not found.");
		} catch (IOException O) {
			logger.writeLog("config.txt cannot be loaded.");
		}

		////////////
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.DATE, -1 * housekeep_days);
		dateCutoff = c.getTime();
		///////////////////////////////////////////////////////////////////////////////////

		try {				
			createFolders(); 			// check and create process folders
			readFTPFolder(); 			// read ftp folder
			getServerVideoList();			// get existing video list
			housekeep();			// housekeeping			 
			fileprocess_loop();			// upload things

			 logger.writeLog("============== Program Finished Successfully ==============", false);
		} catch (Exception O) {
			logger.writeLog("Main Process failed with reason: " +O );
			logger.writeLog("============== Program Failed ==============" );
		}

	}
	
	private static void createFolders() {	
		for (String foldername : folderRequired) {
			File theDir = new File(ftp_folder_path, foldername);
			if (!theDir.exists()) {
				try {
					theDir.mkdir();
				} catch (SecurityException O) {
					logger.writeLog("Failed creating directory " + foldername + " : " + O);
				}
			} else {
				// already exists
			}
		}
	}

	private static void readFTPFolder() {
		logger.writeLog("readFTPFolder Start ");
		File[] filelist = new File(ftp_folder_path, incomingFolderName).listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".xml");
			}
		});

		for (File thefile : filelist) {
//			String cleanname = thefile.getName().split(".xml")[0]; // remove .xml
			try {
				// parse xml
				DocumentBuilder sBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				Document doc = sBuilder.parse(thefile);
				doc.getDocumentElement().normalize(); // node CMS

				Icable_xml_object thexmlObject = new Icable_xml_object( thefile.getName(), doc);
//				 logger.writeLog(thexmlObject.toString());
				xmlreadList.add(thexmlObject);

			} catch (Exception O) {
				logger.writeLog("Package Process error " + O);
			}
		}
		
	}

	private static void getServerVideoList() {
		getVideoJson();
	}

	private static void housekeep() {
		logger.writeLog("Housekeep Start ");
		for (int storyID : existingVid.keySet()) {
			try {
				String utvtube_vodid = existingVid.get(storyID).getVodid();
				String strExtLastUpdate = "" + existingVid.get(storyID).getLastUpdateDate();
				Date dateExistingLastUpdate = icableformat.parse(strExtLastUpdate);

				if (dateCutoff.compareTo(dateExistingLastUpdate) > 0) {
					logger.writeLog("Deleting " + utvtube_vodid + " Date cutoff: " + icableformat.format(dateCutoff)
							+ " , lastupdate: " + strExtLastUpdate);
					deleteVideoAPICall(utvtube_vodid);
				} else {
					logger.writeLog("Keep  " + utvtube_vodid + " Date cutoff: " + icableformat.format(dateCutoff)
							+ " , lastupdate: " + strExtLastUpdate);
				}

			} catch (Exception O) {
				logger.writeLog("Parse Error: " + O);
			}
		}
	}

	private static void fileprocess_loop() {
		logger.writeLog("Process files Start ");

		HashMap<Integer, Icable_xml_object> processedUploadList = new HashMap<Integer, Icable_xml_object>();
		try {
				for (		int i = 0; i< xmlreadList.size(); i++) {
					// retain only the newest version of video
					Icable_xml_object theObject = xmlreadList.get(i) ;
				
					int storyID = Integer.parseInt(theObject.getID());
					logger.writeLog("Processing Story: " + theObject.toString());
		
					long updateTime = theObject.getLastUpdateDateInt();				
					
					Date dateExistingLastUpdate = icableformat.parse(theObject.getLastUpdateDate());
					
					if (dateCutoff.compareTo(dateExistingLastUpdate) > 0) {
						 logger.writeLog("story older than cutoff time ");
						moveFile_skipped(theObject);
						continue;
					}
					
					if (existingVid.containsKey(storyID)) {
						Long existingVidUpdatetime = existingVid.get(storyID).getLastUpdateDate();
						String utvtube_vodid = existingVid.get(storyID).getVodid();
		
						if (updateTime <= existingVidUpdatetime) {
							 logger.writeLog("Newer version of story already exists");
							moveFile_skipped(theObject);
							continue;
						} else {
							if (!utvtube_vodid.equals("")) {
								 logger.writeLog("Story exists in utvtube but is older, delete old version");
								deleteVideoAPICall(utvtube_vodid);
							} else {
								 logger.writeLog("Story exists in uploadqueue, replace old in queue");
								Icable_xml_object oldObject = xmlreadList.get ( existingVid.get(storyID).getXmlListIndex() ) ;
								moveFile_skipped(oldObject);
							}
						}
					}
					existingVid.put(storyID, new Utvtube_video_object(storyID, "", "", updateTime,  i ));
					processedUploadList.put(storyID, theObject);
		
				}
				
					logger.writeLog("Start uploading Files ");			
					for (int index : processedUploadList.keySet()) {
						Icable_xml_object theObject = processedUploadList.get(index);
						// upload objects in processed upload list
						logger.writeLog("uploadFile : " + theObject.getID());
						int result = uploadFileToUtvtube(theObject);
						if (result >0 ) { // success
							moveFile_success(theObject);
						}else { // failed
							moveFile_failed(theObject);
						}
					}
				
		} catch (Exception O){
			logger.writeLog(" Process File Failed " +O);
		}

	}

	private static void moveFile_success(Icable_xml_object theObject ) {
		moveFilesProcess( 1, theObject );
	}
	
	private static void moveFile_skipped(Icable_xml_object theObject ) {
		moveFilesProcess( 2 , theObject);
	}
	
	private static void moveFile_failed(Icable_xml_object theObject ) {
		moveFilesProcess( 3 , theObject);
	}
	
	private static boolean moveFilesProcess(int status, Icable_xml_object theObject ) {
		File incomePath =  new File(ftp_folder_path, incomingFolderName);				
		File destinationPath =  new File(ftp_folder_path, folderRequired[ status ]);

		try {
			File xmlFilePath =			new File ( incomePath, 			theObject.getXmlFilename() );    		
			xmlFilePath.renameTo(	new File ( destinationPath , 	theObject.getXmlFilename() )) ;
			
			if ( ! theObject.isText()) {	// not text only	
				File vidFilePath =			 	new File ( incomePath, 		theObject.getVideo().getFileName() );
				vidFilePath.renameTo(	new File ( destinationPath, theObject.getVideo().getFileName()  )) ;
 		
				File imgFilePath = 			new File ( incomePath,  		theObject.getImage().getFileName());	
				imgFilePath.renameTo(	new File ( destinationPath, theObject.getImage().getFileName() )) ;
				
			}
			
		} catch (Exception O) {
			logger.writeLog("Move Files failed : " +O );
		} 				
		return true;
	}


	private static void getVideoJson() {
		try {
			String apiString = "vod/vidid_list";
			URI fullpath = new URL(new URL(api_server_url), apiString).toURI();
			logger.writeLog("[APICall] getVideoJson post URL:  " + fullpath);

			HttpClient httpclient = HttpClients.createDefault();
			HttpPost httppost = new HttpPost(fullpath);

			HttpResponse response = httpclient.execute(httppost);
			HttpEntity postresult = response.getEntity();

			if (postresult != null) {
				BufferedReader breader = new BufferedReader(new InputStreamReader(postresult.getContent(), "UTF-8"));
				try {
					JSONObject jsonresult = new JSONObject(EntityUtils.toString(postresult, "UTF-8"));
					JSONArray theResultArray = jsonresult.getJSONArray("rows");

					logger.writeLog("[APICall] getVideoJson Result:");

					for (int i = 0; i < jsonresult.getInt("total"); i++) {
						JSONObject theRecord = theResultArray.getJSONObject(i);

						String theFilename = theRecord.getString("filename");

						// parse Filename eg 6220220180305222842_5aaf851d35a579.33404027
						String splittedString = (theFilename.split("_"))[0];
						if (splittedString.matches("[0-9]+")) {
							int dateLoc = splittedString.length() - 14;
							int storyID = Integer.parseInt(splittedString.substring(0, dateLoc));
							long updateTime = Long.parseLong(splittedString.substring(dateLoc));
							// logger.writeLog("iCable video : "+ theRecord.getString("id") +": iCableID:
							// "+ iCableID +", updateTime: "+ updateTime );

							if (existingVid.containsKey(storyID)) {
								Long existingVidUpdatetime = existingVid.get(storyID).getLastUpdateDate();
								if (updateTime <= existingVidUpdatetime) {
									continue;
								}
							}
							existingVid.put(storyID, new Utvtube_video_object(storyID, theRecord.getString("id"),
									theRecord.getString("categories_id"), updateTime , -1));

						} else {
							// logger.writeLog("Not iCable. "+ theRecord.getString("id") +", Filename: "+
							// theFilename );
						}

					}

				} finally {
					breader.close();
				}
			}
		} catch (Exception O) {
			logger.writeLog("[APICall] getVideoJson called Failed  : " + O);
		}
	}

	private static int uploadFileToUtvtube(Icable_xml_object theStory) {
		try {

			File incomingPath = new File(ftp_folder_path, incomingFolderName);
			String apiString = "vod/upload_file";
			URI fullpath = new URL(new URL(api_server_url), apiString).toURI();
			logger.writeLog("[APICall] uploadFileToUtvtube post URL:  " + fullpath);

			HttpClient httpclient = HttpClients.createDefault();
			HttpPost httppost = new HttpPost(fullpath);

			// Story
			int theStoryID = Integer.parseInt(theStory.getID());
			long theUpdatetime = Long.parseLong(theStory.getLastUpdateDate());
			String newVidname = theStory.getAppendVidname();
			String newImgname = theStory.getAppendImgname();

			HttpEntity theentity = MultipartEntityBuilder.create().build();
			// check if story have video
			if (theStory.isText()) {
				theentity = MultipartEntityBuilder.create()
						.addTextBody("title", theStory.getStory().getHeadline(), ContentType.TEXT_PLAIN.withCharset("UTF-8"))
						.addTextBody("description", theStory.getStory().getStoryText(), ContentType.TEXT_PLAIN.withCharset("UTF-8"))
						.addTextBody("categories_id", ""+theStory.getCategories_id(), ContentType.TEXT_PLAIN.withCharset("UTF-8"))
						
						.addTextBody("originfilename", newVidname)
						.build();
			} else {
				theentity = MultipartEntityBuilder.create()
						.addTextBody("title", theStory.getStory().getHeadline(), ContentType.TEXT_PLAIN.withCharset("UTF-8"))
						.addTextBody("description", theStory.getStory().getStoryText(), ContentType.TEXT_PLAIN.withCharset("UTF-8"))
						.addTextBody("originfilename", theStory.getVideo().getFileName(), ContentType.TEXT_PLAIN.withCharset("UTF-8"))
						.addTextBody("categories_id", ""+theStory.getCategories_id(), ContentType.TEXT_PLAIN.withCharset("UTF-8"))
						
						.addBinaryBody("vidupload", new File(incomingPath, theStory.getVideo().getFileName()),
								ContentType.create("application/octet-stream"), newVidname)
						.addBinaryBody("imgupload", new File(incomingPath, theStory.getImage().getFileName()),
								ContentType.create("application/octet-stream"), newImgname)
						.build();
			}
			
			httppost.setEntity(theentity);
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity postresult = response.getEntity();

			if (postresult != null) {
				try {
					JSONObject jsonresult = new JSONObject(EntityUtils.toString(postresult, "UTF-8"));						
					JSONObject theResultArray = jsonresult.getJSONObject("result");

					String newVodid = "" + theResultArray.getInt("vodid");
					// logger.writeLog("uploadFileURL Result = "
					// +"vodid: "+ theResultArray.getInt("vodid")
					// +", filename: "+ theResultArray.getString("filename")
					// +", duration: "+ theResultArray.getString("duration")
					// +", error: "+ theResultArray.getBoolean("error")
					// );

					existingVid.put(theStoryID, new Utvtube_video_object(theStoryID, newVodid, "", theUpdatetime, -1));

					return theResultArray.getInt("vodid");

				} finally {
				}

			}

		} catch (Exception O) {
			logger.writeLog("[APICall] uploadFileURL called Failed  : " + O);
			return -1;
		}
		return -1;
	}

	private static void deleteVideoAPICall(String utvtube_vidid) {
		try {
			String apiString = "vod/delete_video";
			URI fullpath = new URL(new URL(api_server_url), apiString).toURI();
			logger.writeLog("[APICall] deleteVideoAPICall post URL:  " + fullpath);

			HttpClient httpclient = HttpClients.createDefault();
			HttpPost httppost = new HttpPost(fullpath);

			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("id", "" + utvtube_vidid));
			httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));

			HttpResponse response = httpclient.execute(httppost);
			HttpEntity postresult = response.getEntity();

			if (postresult != null) {
				logger.writeLog("[APICall] deleteVideoAPICall Done");
			}
		} catch (Exception O) {
			logger.writeLog("[APICall] deleteVideoAPICall called Failed  : " + O);
		}
	}

}
