
public class icable_xml_object_story{
	
		private String Headline, StoryText;
		
		public icable_xml_object_story(	String _Headline, String _StoryText) {			
			Headline = _Headline;
			StoryText = _StoryText;
		}
		
		public String getHeadline() {
			return Headline;
		}

		public String getStoryText() {
			return StoryText;
		}		
}