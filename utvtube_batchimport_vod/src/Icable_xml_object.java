import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class Icable_xml_object {

	private String xmlFilename;

	private String Category, Pubdate, LastUpdateDate, DataStatus, ID, Provider;
	private icable_xml_object_story Story;
	private icable_xml_object_media Image, Video;

	public Icable_xml_object() {
	}

	public Icable_xml_object(String filename, Document doc) {

		xmlFilename = filename;
		Element newsnode = (Element) (doc.getElementsByTagName("News").item(0)); // only 1

		Category = newsnode.getElementsByTagName("Category").item(0).getTextContent();
		Pubdate = newsnode.getElementsByTagName("Pubdate").item(0).getTextContent();
		LastUpdateDate = newsnode.getElementsByTagName("LastUpdateDate").item(0).getTextContent();
		DataStatus = newsnode.getElementsByTagName("DataStatus").item(0).getTextContent();
		ID = newsnode.getElementsByTagName("ID").item(0).getTextContent();
		Provider = newsnode.getElementsByTagName("Provider").item(0).getTextContent();

		Element storynode = (Element) (newsnode.getElementsByTagName("Story").item(0));
		String storyheadline = storynode.getElementsByTagName("Headline").item(0).getTextContent();
		String storytext = storynode.getElementsByTagName("StoryText").item(0).getTextContent();
		Story = new icable_xml_object_story(storyheadline, storytext);

		NodeList medianodeList = newsnode.getElementsByTagName("Video");
		if (medianodeList.getLength() > 0) {
			Element medianode = (Element) (medianodeList.item(0));
			String formatnode = medianode.getElementsByTagName("Format").item(0).getTextContent();
			String filenamenode = medianode.getElementsByTagName("FileName").item(0).getTextContent();
			Video = new icable_xml_object_media(formatnode, filenamenode);

			medianode = (Element) (newsnode.getElementsByTagName("Image").item(0));
			formatnode = medianode.getElementsByTagName("Format").item(0).getTextContent();
			filenamenode = medianode.getElementsByTagName("FileName").item(0).getTextContent();
			Image = new icable_xml_object_media(formatnode, filenamenode);
		} else {
			Video = new icable_xml_object_media();
			Image = new icable_xml_object_media();
		}

	}

	public String toString() {
		String msg = ID + ", " + Category + ", " + LastUpdateDate + ", " + DataStatus + ", " + " | Story: "
				+ getStory().getHeadline(); // + " | Video: " + getVideo().getFileName() + " : " + getAppendFilename();

		if (isText()) {
			msg = "Media  : " + msg;
		} else {
			msg = "Text : " + msg;
		}
		return msg;
	}

	////
	public String getAppendFilename() {
		String theFilename = ID + "_" + LastUpdateDate;
		return theFilename;
	}

	public String getAppendVidname() {
		String theFilename = getAppendFilename() + "." + Video.getFormat();
		return theFilename;
	}

	public String getAppendImgname() {
		String theFilename = getAppendFilename() + "." + Image.getFormat();
		return theFilename;
	}

	public boolean isText() {
		return getVideo().getFileName().equals("");
	}

	public int getCategories_id( ) {
		switch ( Category ) {
		case "Local":		
			return Utvtube_batchimport_vod.categories_id.icable_news;
//			break;		
		case "World":		
			return Utvtube_batchimport_vod.categories_id.icable_news;
//			break;		
		case "Sports":		
			return Utvtube_batchimport_vod.categories_id.icable_news;
//			break;		
		case "Fin":						
			return Utvtube_batchimport_vod.categories_id.icable_finance;
//			break;		
		case "":								
			return Utvtube_batchimport_vod.categories_id.icable_entertainment;
//			break;		
		}
		return -1;
	}
	/////////////////////// getters ///////////////////////////////////
	public String getXmlFilename() {
		return xmlFilename;
	}

	public String getCategory() {
		return Category;
	}

	public String getPubdate() {
		return Pubdate;
	}

	public String getLastUpdateDate() {
		return LastUpdateDate;
	}

	public Long getLastUpdateDateInt() {
		long theresult = Long.parseLong(LastUpdateDate);
		return theresult;
	}

	public String getDataStatus() {
		return DataStatus;
	}

	public String getID() {
		return ID;
	}

	public String getProvider() {
		return Provider;
	}

	public icable_xml_object_story getStory() {
		return Story;
	}

	public icable_xml_object_media getImage() {
		return Image;
	}

	public icable_xml_object_media getVideo() {
		return Video;
	}

	/*
	 * <?xml version="1.0" encoding="UTF-8"?> <CMS> <News>
	 * <Category>Local</Category> <Pubdate>20180302111222</Pubdate>
	 * <LastUpdateDate>20180302131729</LastUpdateDate>
	 * <DataStatus>Update</DataStatus> <ID>522532</ID> <Provider>iCable</Provider>
	 * <Story> <Headline><![CDATA[葉劉倡向無交稅無物業人士派錢]]></Headline>
	 * <StoryText><![CDATA[財政預算案公布後，因為沒有派錢和沒有措施惠及部份低收入階層，備受批評。
	 * 身兼行政會議成員的新民黨主席葉劉淑儀向財政司司長提出修訂建議，向年滿十八歲上年度沒有交稅和沒有物業的香港永久性居民，每人派發三千元現金，
	 * 民主黨更建議派六千元。<p>預算案公布派糖用了五百多億，但沒有直接派錢。事隔兩日，身兼行會成員的新民黨主席葉劉淑儀表示，分析過整份預算案，
	 * 發覺有一群人被遺忘。新民黨建議修訂預算案凡年滿十八歲的香港永久性居民上年度沒有交稅、又沒有物業，毋須繳付差餉，每人可以取得三千元現金，
	 * 他們估計最多一百九十二萬人合資格受惠，涉及開支約六十億元。<p>民主黨更建議派多一倍，要派六千元現金，他強調不支持全民派錢，只是針對中低收入人士。]]><
	 * /StoryText> </Story> <Video> <VideoFile> <Format>mp4</Format>
	 * <FileName>20180302131729_01.mp4</FileName> </VideoFile> </Video> <Image>
	 * <ImageFile> <Format>JPG</Format> <FileName>20180302131729_01.jpg</FileName>
	 * </ImageFile> </Image> </News> </CMS>
	 */

}
