
public class Utvtube_video_object {
	
	int storyID ;
	String  vodid, categories_id ;
	Long LastUpdateDate ;
	int xmlListIndex;

	public Utvtube_video_object(int storyID, String vodid, String categories_id, Long lastUpdateDate, int xmlListIndex) {
		super();
		this.storyID = storyID;
		this.vodid = vodid;
		this.categories_id = categories_id;
		this.xmlListIndex = xmlListIndex;		
		LastUpdateDate = lastUpdateDate;
	}
	
	public int getStoryID() {
		return storyID;
	}
	public String getVodid() {
		return vodid;
	}
	public String getCategories_id() {
		return categories_id;
	}
	public Long getLastUpdateDate() {
		return LastUpdateDate;
	}
	public int  getXmlListIndex() {
		return xmlListIndex;
	}
	
}
