
public class icable_xml_object_media{
	
	private String Format, FileName;
		
		public icable_xml_object_media() {
			Format = "";
			FileName = "";
		}
		
		public icable_xml_object_media(	String _Format, String _FileName) {
			Format = _Format;
			FileName = _FileName;
		}
		
		public String getFormat() {
			return Format;
		}

		public String getFileName() {
			return FileName;
		}
	}