import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;

public class Utv_batchimport_epg {
	
	public class categories_id  {	
		static final int icable_news	= 109; 
		static final int  icable_finance = 108;
		static final int icable_entertainment = 301;
	};
	public class channel_id  {	
		static final int icable_news	= 540; 
		static final int  icable_finance = 541;
		static final int icable_entertainment = 542;
	};

	
	private static String incomingFolderName = "Incoming";
	private static String processedFolderName = "Processed";
	private static String skippedFolderName = "Skipped";
	private static String failedFolderName = "Failed";
	private static String[] folderRequired = { incomingFolderName, processedFolderName, skippedFolderName, failedFolderName };
		
	public static String isdevStr = "_epg";
	private static String api_server_url = "";
	private static String ftp_folder_path = "";

	public static Logger logger = null;

	
	public static void main(String[] args) {

		// program initialize
		logger = Logger.getInstance();

		logger.writeLog("Utvtube batch import start.");
		// read config
		try {
			BufferedReader brReader = new BufferedReader(new FileReader("config-epg.txt"));
			String strRow = brReader.readLine();
			while (strRow != null) {
				String[] result = strRow.split("=");
				if (result.length >= 2) {
					String pname = result[0];
					String pvalue = result[1];
					if (pname.compareTo("ENV") == 0) {
//						isdevStr = pvalue;
					} else if (pname.compareTo("API_SERVER_URL") == 0) {
						api_server_url = pvalue;
					} else if (pname.compareTo("FTP_FOLDER_PATH") == 0) {
						ftp_folder_path = pvalue;
					} 
				}
				strRow = brReader.readLine();
			}
			brReader.close();
		} catch (FileNotFoundException O) {
			logger.writeLog("config.txt is not found.");
		} catch (IOException O) {
			logger.writeLog("config.txt cannot be loaded.");
		}

		try {				
			createFolders(); 			// check and create process folders
			readFTPFolder(); 			// read ftp folder	 

			 logger.writeLog("============== Program Finished Successfully ==============", false);
		} catch (Exception O) {
			logger.writeLog("Main Process failed with reason: " +O );
			logger.writeLog("============== Program Failed ==============" );
		}

	}
	
	private static void createFolders() {	
		Integer[] channelFolder = { categories_id.icable_news, categories_id.icable_finance, categories_id.icable_entertainment };
		
		try {
			for (String foldername : folderRequired) {
				File theDir = new File(ftp_folder_path, foldername);
				if (!theDir.exists()) {			
						theDir.mkdir();
				} else {
					// already exists
				}	
						for (int channelname : channelFolder) {
							File thechannelDir = new File(theDir, ""+channelname);
							if (!thechannelDir.exists()) {			
								thechannelDir.mkdir();
							}
						}
								
			}		
		} catch (SecurityException O) {
			logger.writeLog("Failed creating directory : " + O);
			}
	}

	private static void readFTPFolder() {
		logger.writeLog("readFTPFolder Start ");

		File incomingFolder = new File(ftp_folder_path, incomingFolderName);
		Integer[] channelFolder = { categories_id.icable_news, categories_id.icable_finance, categories_id.icable_entertainment };
		Integer[] channelId = { channel_id.icable_news, channel_id.icable_finance, channel_id.icable_entertainment };
	
		for (int i = 0; i < channelFolder.length ; i++) {
		Integer channelname = channelFolder [i] ;
			
			File channelfolder = new File(incomingFolder, ("" + channelname));

			File[] filelist = channelfolder.listFiles(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return name.toLowerCase().endsWith(".xml");
				}
			});
			ArrayList<icable_epg_xmlobject.Programme> theChannelProgrammes = new ArrayList<icable_epg_xmlobject.Programme> () ;
			
			for (File thefile : filelist) {
				String cleanname = thefile.getName().split(".xml")[0]; // remove .xml
				try {

					// parse xml
					DocumentBuilder sBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
					Document doc = sBuilder.parse(thefile);
					doc.getDocumentElement().normalize(); // node CMS

					icable_epg_xmlobject thexmlObject = new icable_epg_xmlobject( cleanname, doc);
				//	logger.writeLog(thexmlObject.toString());
					
				//	for (int n = 0; n < thexmlObject.getprogramCount(); n++) {
				//		uploadFile(channelId[i], thexmlObject.getProgrammes(n) );						
				//	}
						for (int n = 0; n < thexmlObject.getprogramCount(); n++) {							
							theChannelProgrammes.add(thexmlObject.getProgrammes(n));
					}	
						logger.writeLog("[Process File] Finishing parsing file " + thexmlObject.getXmlFilename());
					moveFile_success(thexmlObject, ""+channelFolder[i] );
				} catch (Exception O) {
					logger.writeLog("Package Process error " + O);
				}
			}
			
			uploadFile(channelId[i], theChannelProgrammes );	
		}

	}


	private static void moveFile_success(icable_epg_xmlobject theObject, String channel ) {
		moveFilesProcess( 1, theObject, channel );
	}
	
	private static void moveFile_skipped(icable_epg_xmlobject theObject, String channel ) {
		moveFilesProcess( 2 , theObject , channel );
	}
	
	private static void moveFile_failed(icable_epg_xmlobject theObject , String channel ) {
		moveFilesProcess( 3 , theObject , channel);
	}
	
	private static boolean moveFilesProcess(int status, icable_epg_xmlobject theObject, String channel ) {
		File baseIncomePath = new File(ftp_folder_path, incomingFolderName);		
		File basedestinationPath =  new File(ftp_folder_path, folderRequired[ status ]);
		
		File incomePath =  new File(baseIncomePath, channel);				
		File destinationPath =  new File(basedestinationPath, channel);		
		try {
			File destinationFile = 		new File ( destinationPath , 	theObject.getXmlFilename() );
			if (destinationFile.exists()) {
				destinationFile.delete();
			}
			File xmlFilePath =			new File ( incomePath, 			theObject.getXmlFilename() ); 			
			xmlFilePath.renameTo(	new File ( destinationPath , 	theObject.getXmlFilename() )) ;		
		} catch (Exception O) {
			logger.writeLog("Move Files failed : " +O );
		} 				
		return true;
	}



	private static void uploadFile(int channelID, 	ArrayList<icable_epg_xmlobject.Programme> theChannelProgrammes) {
		try {
			
				JSONArray data_array = new JSONArray();
				for (int n = 0; n < theChannelProgrammes.size();  n++) {
					JSONObject data_obj = new JSONObject();
					 data_obj.put("lc_num", "" + channelID);
					 data_obj.put("showdate", theChannelProgrammes.get(n).getDBShowdate() );
					 data_obj.put("starttime", theChannelProgrammes.get(n).getStarttime() );
					 data_obj.put("duration", theChannelProgrammes.get(n).getDuration() );
					 data_obj.put("program_ch",  theChannelProgrammes.get(n).getProgramme_name_chi() );
					 data_obj.put("program_en", theChannelProgrammes.get(n).getProgramme_name_eng() );
					 data_array.put( data_obj);						
				}
//				logger.writeLog("[APICall] channelID is: \n " + data_array.toString());
				logger.writeLog("[APICall] Processing channelID : \n " + channelID);

				
			String apiString = "livechannel/update_program_list";
			URI fullpath = new URL(new URL(api_server_url), apiString).toURI();
//			logger.writeLog("[APICall] uploadFile post URL:  " + fullpath);

			HttpClient httpclient = HttpClients.createDefault();
			HttpPost httppost = new HttpPost(fullpath);

			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("data_array",  data_array.toString() ));

				httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));

			HttpResponse response = httpclient.execute(httppost);
			HttpEntity postresult = response.getEntity();

			if (postresult != null) {
//				logger.writeLog("[APICall] uploadFile Done");
			}
		} catch (Exception O) {
			logger.writeLog("[APICall] uploadFile called Failed  : " + O);
		}
	}

	private static void logic() {
	}

}
