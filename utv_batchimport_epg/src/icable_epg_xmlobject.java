import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class icable_epg_xmlobject {
	
	public static SimpleDateFormat xmlFilenameFormat = new SimpleDateFormat("yyyyMMdd");
	public static SimpleDateFormat xmlTimeformat = new SimpleDateFormat("HH:mm");
	public static SimpleDateFormat fullTimeformat = new SimpleDateFormat("yyyyMMdd HH:mm");
	public static SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd");

	private String xmlFilename;

	private ArrayList<Programme> programList  = new ArrayList<Programme>();
	
	public class Programme{		
		private String showdate, starttime, duration; 	
		private String 	programme_name_chi, programme_name_eng;	
		
		public Programme(String showdate, String starttime, String programme_name_chi, String programme_name_eng) {
			super();
			this.showdate = showdate;
			this.starttime = starttime;			
			this.programme_name_chi = programme_name_chi;
			this.programme_name_eng = programme_name_eng;
		}	
		
		public void  setDuration(String duration) {
			this.duration = duration;
		}
		
		public String getShowdate() {
			return showdate;
		}	
		public String getDBShowdate() {
		try {
			Date EPGDate = xmlFilenameFormat.parse(showdate);			
			String DBDate = dbDateFormat.format( EPGDate) ;
			return DBDate;
		
		}catch (Exception e) {
			return showdate;
		}
	
		}	
		public String getStarttime() {
			return starttime;
		}	
		public String getDuration() {
			return duration;
		}
		public String getProgramme_name_chi() {
			return programme_name_chi;
		}
		public String getProgramme_name_eng() {
			return programme_name_eng;
		}
	}
	
	public icable_epg_xmlobject() {
	}

	public icable_epg_xmlobject(  String filename, Document doc) {
try {
		this.xmlFilename = filename;
		
		Element recordnode = (Element) (doc.getElementsByTagName("record").item(0)); // only 1
		NodeList programmenode = doc.getElementsByTagName("programme"); 
		ArrayList <String> startTimeArray = new ArrayList<String> ();
		
		for ( int i = 0; i < programmenode.getLength() ; i++) {
		
			Element programNode = (Element) (programmenode.item(i));
		
			String starttime = programNode.getElementsByTagName("time").item(0).getTextContent();	
			String session_mark = programNode.getElementsByTagName("session_mark").item(0).getTextContent();			
			String programme_name_chi = programNode.getElementsByTagName("programme_name_chi").item(0).getTextContent();
			String programme_name_eng = programNode.getElementsByTagName("programme_name_eng").item(0).getTextContent();
			
			String appendTime = xmlFilename+" "+starttime;
		
			Date EPGDate = fullTimeformat.parse(appendTime);			
			Calendar c  = Calendar.getInstance();
			c.setTime(EPGDate);
					
			switch (session_mark) {
			case "AM":				
				break;		
			case "PM":
				if ( c.get(Calendar.HOUR_OF_DAY) < 12 ) {
					c.add(Calendar.HOUR, 12 );
				}
				break;			
			case "NM":
				c.add(Calendar.DATE, 1 );
					break;			
		}
					
			Date epgDateplus = c.getTime();
			String realStarttime = fullTimeformat.format( epgDateplus) ;
			String newStartdate = xmlFilenameFormat.format( epgDateplus) ;
			String newStarttime = xmlTimeformat.format( epgDateplus) ;
					
			startTimeArray.add(realStarttime);
			Programme theprogramme = new Programme( newStartdate, newStarttime,   programme_name_chi,  programme_name_eng) ;
			programList .add (theprogramme);
		}	
			
		Date EPGDate = xmlFilenameFormat.parse(xmlFilename);			
		Calendar c  = Calendar.getInstance();
		c.setTime(EPGDate);
		c.add(Calendar.DATE, 1 );
		c.add(Calendar.HOUR, 6 );	
		
		startTimeArray.add ( fullTimeformat.format(c.getTime()) );
		
		for ( int i = 0; i < programList.size() ; i++) {			
			
			Date startDate = fullTimeformat.parse( startTimeArray.get(i) );			
			Date endDate = fullTimeformat.parse( startTimeArray.get( i+1) );			
			
			// calc duration
			long  seconddiff = (endDate.getTime() - startDate.getTime()) / 1000;
			long hourdiff = seconddiff / 3600; 
			long minutediff =   (seconddiff % 3600) / 60;
			String durationString = String.format(  "%02d:%02d",hourdiff,  minutediff) ; 			
			programList.get(i).setDuration(durationString);
			
		}
	}catch (Exception O) {
		
	}	
		
	}


public String toString() {
	String msg = "Xml: "+ xmlFilename+" : ";
	
	for ( int i = 0; i < programList.size() ; i++) {			
		msg += " start= "	+programList.get(i).getShowdate() +" " +programList.get(i).getStarttime();
		msg += ",  duration= "	+programList.get(i).getDuration();	
		msg += ",  name= "	+programList.get(i).getProgramme_name_chi();
		msg += "\n";
	}
	return msg;
	
}

public int getprogramCount() {
	return programList.size();
}

public Programme  getProgrammes(int i ) {
	return programList.get(i);
}


public String getXmlFilename() {
	return xmlFilename+".xml";
}

	/* 
	  D:\NVideo\iCable-EPG\301\20180322.xml
 <record chid="">
 <programme>
  <session_mark>AM</session_mark>
  <time>06:00</time>
  <programme_name_chi>�T�ַs�D</programme_name_chi>
  <programme_name_eng>E-news Cast 1 Epi. 21/03/2018</programme_name_eng>
  <remarks></remarks>
 </programme>
 <programme>
  <session_mark>AM</session_mark>
  <time>06:15</time>
  <programme_name_chi>�T�ַs�D</programme_name_chi>
  <programme_name_eng>E-news Cast 2 Epi. 21/03/2018</programme_name_eng>
  <remarks></remarks>
 </programme>
  </record> 
  */
}
